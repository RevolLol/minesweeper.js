/**
 * cell states: 0 - closed, 1 - opened, 2 - marked
 * isMine - indicates if cell has a mine or not
 */
 
function Cell() {
	let nState = 0, bisMine = false;

	this.getState = function() {
		return nState
	}

	this.setState = function(nNewState) {
		if(nNewState >= 0 && nNewState < 3) {
			nState = nNewState;
			return this
		}
		throw new Error("State has to be a number in range of [0,2]");
	}

	this.hasMine = function(){
		return bisMine
	}

	this.setMine = function(bMine){
		if(typeof bMine === "boolean") {
			bisMine = bMine;
			return this
		}
		throw new Error("Argument bMine must be a boolean");
	}
}

Cell.prototype.open = function(){
	if(this.getState === 0) {
		this.setState(1);
		return this.hasMine
	}
	return false
}

Cell.prototype.markMine = function(){
	//checking if cell wasnt yet opened
	const nState = this.getState();
	if(nState !== 1) {
		this.setState(2);
		return this
	}
}

Cell.states = ["closed", "opened", "marked"];